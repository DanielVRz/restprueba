package com.techuniversity.rest1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(path = "/test")
public class TestController {

    @GetMapping("/saludos")
    public String saludar(@RequestParam(value="nombre", defaultValue = "Tech U!", required = true) String name){
        return String.format("Saludos %s", name);
    }

    @GetMapping("/varios")
    public String varios(@RequestParam Map<String, String> allParams) {
        return "Los Parametros son: " + allParams.entrySet();
    }

    @GetMapping("/")

}
